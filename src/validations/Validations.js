let valid = true;

export const ValidateField = (fieldName, value) => {
  switch (fieldName) {
    case "username":
      return validateUsername(value);
      break;
    case "password":
      return validatePassword(value);
      break;
    case "email":
      return validateEmail(value);
      break;

    default:
      return "";
  }
};

export const validateUsername = (username) => {
  console.log("username ", username);
  if (username?.trim() === "" || username == undefined) {
    // console.log("username1 ",username)
    return "Username is required";
  } else {
    return "";
  }
};

const validatePassword = (password) => {
  if (password?.trim() === "" || password == undefined) {
    return "Password is required";
  }

  return "";
};

const validateEmail = (email) => {
  if (email?.trim() === "" || email == undefined) {
    return "Email is required";
  }
  return "";
};

export const validateForm = (formData) => {
  const newErrors = {
    username: validateUsername(formData?.username),
    password: validatePassword(formData?.password),
    email: validateEmail(formData?.email),
  };

  if (newErrors?.username || newErrors?.password || newErrors?.email) {
    console.log("newErrors ", newErrors);
    valid = false;
  }
  return { valid, errors: newErrors };
};
