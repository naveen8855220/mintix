import React from "react";
import Footer from "./components/Footer";
import Home from "./components/Home";
import { Routes, Route } from "react-router-dom";
import "./saas/style.scss";
import Navbar from "./components/Navbar";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Events from "./components/Events";
import Checkout from "./components/Checkout";
import Return from "./components/Return";
import { BrowserRouter } from "react-router-dom";
import PasswordReset from "./components/PasswordReset";

const App = () => {
  return (
    <div className="bg-main">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/signup" element={<Signup />}></Route>
          <Route path="/event/:id" element={<Events />}></Route>
          <Route path="/checkout" element={<Checkout />}></Route>
          <Route path="/return" element={<Return />}></Route>
          <Route path="/reset-password" element={<PasswordReset/>}></Route>
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
};

export default App;
