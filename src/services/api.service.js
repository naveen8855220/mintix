import Parse from "parse";
import { auth } from "./index.js";

const Event = Parse.Object.extend("Event");
const eventData = new Parse.Query(Event);
const Coupon = Parse.Object.extend("Coupon");
const Booking = Parse.Object.extend("Booking");

export const getEvents = async () => {
  let event = [];
  try {
    const res = await eventData.find();
    for (let i of res) {
      const Id = i?.id;
      let obj = { ...i?.attributes, Id: Id };
      event.push(obj);
    }
    return event;
  } catch (error) {
    console.error("Get Events Error ", error);
  }
};

export const getSingleEvent = async (Id) => {
  let event = null;
  try {
    const res = await eventData.find();
    for (let i of res) {
      if (i.id) {
        event = i?.attributes;
        return event;
      }
    }
  } catch (error) {
    console.log("single data error ", error);
  }
};

export const eventTicketUpdate = async (id, total) => {
  console.log("total ", total);
  console.log("id ", id);

  const max_ticket_total = total.toString();

  try {
    const query = new Parse.Query(Event);
    query.limit(1);
    query.equalTo("objectId", id);

    const eventObject = await query.first();

    if (eventObject) {
      eventObject.set("max_tickets", max_ticket_total);
      const res = await eventObject.save();
      return res.toJSON();
    } else {
      console.log("event object is ", eventObject);
    }
  } catch (error) {
    console.log("error ", error);
  }
};

export const addCoupon = async (code) => {
  try {
    const query = new Parse.Query(Coupon);
    query.limit(1);
    query.equalTo("code", code);
    const res = await query.find();
    console.log("res ", res[0].toJSON());

    return res[0]?.toJSON();
  } catch (error) {
    console.error("add Coupon Error ", error);
  }
};

export const createBooking = async (eventId, total, coupon) => {
  const booking = new Booking();
  const event = new Event();

  const currentuser = auth?.currentUser();
  event.id = eventId;
  const obj = {
    userId: currentuser,
    status: "open",
    amount: total,
    coupon: coupon,
    eventId: event,
  };

  console.log("booking details from client ",obj)

  try {
    const res = await booking.save(obj);
    console.log("booking response from api ", res.toJSON());
    return res.toJSON();
  } catch (error) {
    console.log("booking response error ", error);
  }
};

export default {
  getEvents,
  getSingleEvent,
  addCoupon,
  createBooking,
  eventTicketUpdate,
};
