import Parse from "parse";

//Fetching the current signin User
export const currentUser = () => {
  return Parse.User.current();
};

//signIn User
export const signInUser = async (userData) => {
  const { username, password } = userData;
  const userObj = {
    username: username,
    password: password,
  };

  try {
    const res = await Parse.User.logIn(userObj?.username, userObj?.password);
    return res;
  } catch (error) {
    console.error("signInUser Error ", error);
  }
};

//SignUp user
export const signUpUser = async (data) => {
  const { username, password, email } = data;
  const user = new Parse.User();
  const userObj = {
    username: username,
    password: password,
    email: email,
  };
  try {
    const res = await user.signUp(userObj);
  } catch (error) {
    console.log("signUpUser Error ", error);
  }
};

//SignOut User
export const SignOut = async () => {
  try {
    const res = await Parse.User.logOut();
  } catch (error) {
    console.error("SignOut Error ", error);
  }
};

export const passwordReset = async (email) => {
  try {
    const res = Parse.User.requestPasswordReset(email);
    return res;
  } catch (error) {
    console.log("reset password error ", error);
  }
};

export default { currentUser, signInUser, SignOut, signUpUser, passwordReset };
