import { create } from "zustand";
import { devtools,persist } from "zustand/middleware";

const createStore = (set)=>{
    return({
        totalPrice:0,
        isLogged:false,
        userEmail:null,
        
        addTotal : (value)=>set((state)=>({totalPrice:state?.totalPrice+value})),
        
        logged:(value)=>set((state)=>({isLogged:value}))
    })
}


export const usemyStore = create(devtools(persist(createStore,{name:'prices'})))


// export default usemyStore;

