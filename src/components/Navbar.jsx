import React, { useEffect, useState } from "react";
import logo from "../Images/logo.svg";
import { Link, useNavigate } from "react-router-dom";
import userperson from "../icons/userperson.svg";
import { auth } from "../services";
import { usemyStore } from "../store/store";

const Navbar = () => {
  const [login, setLogin] = useState(false);
  const [user, setUser] = useState();

  const store = usemyStore((state) => state);
  const { isLogged, logged } = store;

  useEffect(() => {
    const res = auth.currentUser();
    if (res) {
      console.log("user details ", res.toJSON());
      setUser(res.toJSON());
      setLogin(true);
    } else {
      console.log("no user ", res);
      setLogin(false);
    }
  }, [login]);

  const handleSignOut = async () => {
    const res = await auth.SignOut();
    setLogin(false);
    logged(false);
  };

  const loggedIn = () => {
    return (
      <>
        <ul className="navbar-nav me-auto mb-2 mb-lg-0 w-100">
          <li className="nav-item">
            <Link className="nav-link fs-5" to={"/login"}>
              <img height={22} src={userperson} className="px-2" alt="User" />{" "}
              Log Inn
            </Link>
          </li>
        </ul>
      </>
    );
  };

  const loggedOut = () => {
    return (
      <>
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
            <a
              className="nav-link dropdown-toggle fs-4 fw-bold"
              href="#"
              role="button"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              {user?.username}
            </a>
            <ul className="dropdown-menu fs-5">
              <li>
                <a className="dropdown-item" href="#">
                  Mine billetter
                </a>
              </li>
              <li>
                <hr className="dropdown-divider" />
              </li>
              <li>
                <a className="dropdown-item" href="#">
                  Profil
                </a>
              </li>
              <li onClick={handleSignOut}>
                <a className="dropdown-item" href="#">
                  Logg ut
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </>
    );
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg">
        <div className="container">
          <Link to={"/"}>
            <img src={logo} alt="mintix" />
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 w-100">
              <li className="nav-item me-auto">
                <a
                  className="nav-link active fs-5 mx-3"
                  aria-current="page"
                  href="#"
                >
                  Hva er Mintix?
                </a>
              </li>
              <li className="nav-item">{login ? loggedOut() : loggedIn()}</li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
