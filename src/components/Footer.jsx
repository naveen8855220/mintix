import React from "react";
import logo from "../Images/logo.svg";
import facebook from "../icons/facebook.svg";
import Instagram from "../icons/instagram.svg";

const Footer = () => {
  return (
    <div className="bg-footer">
      <div className="footer py-5">
        <img src={logo} alt="" />
        <br />
        <div className="py-3">
          <a className="px-2" href="">
            <img src={facebook} width="3%" alt="" />
          </a>
          <a href="">
            <img src={Instagram} alt="" width="3%"/>
          </a>
        </div>
        <a href="" className="text-dark">Vilkår for bruk</a>
        <br />
        <br />
        <p>©2024 Mintix Norge. Alle rettigheter reservert.</p>
      </div>
    </div>
  );
};

export default Footer;
