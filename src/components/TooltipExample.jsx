import Button from 'react-bootstrap/Button';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';

function TooltipPositionedExample() {
  return (
    <>
      {['right'].map((placement) => (
        <OverlayTrigger
          key={placement}
          placement={placement}
          overlay={
            <Tooltip id={`tooltip-${placement}`} style={{backgroundColor:'white'}}>
            tax is determined by billing information
            </Tooltip>
          }
        >
          <Button variant="secondary">{placement}</Button>
        </OverlayTrigger>
      ))}
    </>
  );
}

export default TooltipPositionedExample;