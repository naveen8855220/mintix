import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { api } from "../services";
import calender from "../icons/calender.svg";
import clock from "../icons/clock.svg";
import microphone from "../icons/microphone.svg";
import location from "../icons/location.svg";
import { usemyStore } from "../store/store";
import { auth } from "../services";

const Events = () => {
  const { id } = useParams();
  // console.log("id ", id);

  const [event, setEvent] = useState({});
  const [ordinaryCount, setordinaryCount] = useState(0);
  const [studentCount, setStudentCount] = useState(0);
  const [LedsagarCount, setLedsagarCount] = useState(0);
  const [Delsum, setDelsum] = useState(0);
  const [count, setCount] = useState(0);
  const [total, setTotal] = useState(0);
  const [coupon, setCoupon] = useState("");
  const [couponData, setCouponData] = useState({});
  const [discountPrice, setDiscountprice] = useState(0);
  const [toggle, setToggle] = useState(false);

  const ordernum = parseInt(event?.ticket_types?.Ordinær);
  const studentnum = parseInt(event?.ticket_types?.["Student & U18"]);
  const ledsagarnum = parseInt(event?.ticket_types?.Ledsager);
  const store = usemyStore((state) => state);
  const { totalPrice, addTotal } = store;

  const navigate = useNavigate();

  useEffect(() => {
    const fetchSingleEvent = async () => {
      try {
        const res = await api.getSingleEvent(id);
        // console.log("fetchSingleEvent res ", res);
        if (res) {
          setEvent(res);
        }
      } catch (error) {
        console.error("error ", error);
      }
    };

    fetchSingleEvent();
  }, []);

  const fetchDate = (item) => {
    const date = new Date(item);
    const dateString = date.toDateString();
    return dateString;
  };

  const handleChange = (e) => {
    setCoupon(e.target.value);
  };

  const getDiscount = (coupondata, subTotal) => {
    const discount = coupondata?.amount * subTotal;
    const val = total - discount;
    setTotal(val);
    setDiscountprice(discount);
  };

  const showDiscount = () => {
    console.log("You saved ", total);
  };

  const caliclulateDiscount = (coupondata) => {
    console.log("coupodeti ", coupondata);

    //Ordinary with type0 type1
    if (ordinaryCount > 0 && studentCount <= 0) {
      if (coupondata?.type != 2) {
        const subTotal = ordinaryCount;
        getDiscount(coupondata, subTotal);
      } else {
        showDiscount();
      }
    }

    //studnet with type0 type2
    if (studentCount > 0 && ordinaryCount <= 0) {
      if (coupondata?.type != 1) {
        const subTotal = studentCount;
        getDiscount(coupondata, subTotal);
      } else {
        showDiscount();
      }
    }

    //if ordinary and student are there
    if (studentCount > 0 && ordinaryCount > 0) {
      if (coupondata?.type != 2) {
        if (coupondata?.type === 0) {
          const subTotal = ordinaryCount + studentCount;
          getDiscount(coupondata, subTotal);
        }
        if (coupondata?.type === 1) {
          const subTotal = ordinaryCount;
          getDiscount(coupondata, subTotal);
        }
      }
      if (coupondata?.type === 2) {
        const subTotal = studentCount;
        getDiscount(coupondata, subTotal);
      }
    }
  };

  const handleCouponApply = async (e) => {
    e.preventDefault();
    try {
      const res = await api.addCoupon(coupon);
      setCouponData(res);
      // console.log("res ",res)

      caliclulateDiscount(res);
    } catch (error) {
      console.error("Coupon apply Error", error);
    }
  };

  const handleOrdinaryIncrease = () => {
    if (count < 30) {
      setCount((prev) => prev + 1);
      setordinaryCount((prev) => prev + 1);
      setDelsum((prev) => prev + ordernum);
      setTotal((prev) => prev + ordernum);
    }
    return;
  };

  const handleOrdinaryDecrease = () => {
    setCount((prev) => prev - 1);
    setordinaryCount((prev) => prev - 1);
    setDelsum((prev) => prev - ordernum);
    setTotal((prev) => prev - ordernum);
  };

  const handleStudentIncrease = () => {
    if (count < 30) {
      setCount((prev) => prev + 1);
      setStudentCount((prev) => prev + 1);
      setDelsum((prev) => prev + studentnum);
      setTotal((prev) => prev + studentnum);
    }
  };

  const handleStudentDecrease = () => {
    setCount((prev) => prev - 1);
    setStudentCount((prev) => prev - 1);
    setDelsum((prev) => prev - studentnum);
    setTotal((prev) => prev - studentnum);
  };

  const handleLedsagarIncrease = () => {
    if (LedsagarCount < 1) {
      setCount((prev) => prev + 1);
      setLedsagarCount((prev) => prev + 1);
      setDelsum((prev) => prev + ledsagarnum);
      setTotal((prev) => prev + ledsagarnum);
    }
  };

  const handleLedsagarDecrease = () => {
    if (LedsagarCount === 1) {
      setCount((prev) => prev - 1);
      setLedsagarCount((prev) => prev - 1);
      setDelsum((prev) => prev - ledsagarnum);
      setTotal((prev) => prev - ledsagarnum);
    }
  };

  const handleCheckout = async () => {
    //getting current user
    const res = auth.currentUser();
    const currentUser = res.toJSON();

    try {
      //getting max_tickets of event
      const resposne = await api.getSingleEvent(id);
      let max_tickets_count = resposne?.max_tickets;

      //creating new Booking
      const res = await api.createBooking(id, total, coupon);

      if (res?.objectId) {
        const result = await api.eventTicketUpdate(id, max_tickets_count-count);
        console.log("result ticket udpate ", result);
        const data = {
          email: currentUser?.email,
          amount: total,
          bookingId: res?.objectId,
        };
        navigate("/checkout", {
          state: data,
        });
      } else {
        navigate("/login");
      }
    } catch (error) {
      console.log("error ", error);
    }
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-md-10 col-12 offset-md-1 text-center">
            <img src={event?.cover_photo} alt="" className="img-fluid" />
            <h1 className="fw-normal my-3">{event?.title}</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 col-12">
            <div className="d-flex">
              <img src={calender} height={20} alt="" className="mx-2" />
              <p className="mb-0">
                <b>
                  <i>Dato:</i>
                </b>
                &nbsp;{fetchDate(event?.date)}
              </p>
            </div>
            <div className="d-flex">
              <img src={clock} height={20} alt="" className="mx-2" />
              <p className="m-0">
                <span className="fw-bold">Dører åpner</span>
              </p>
            </div>
            <div className="d-flex">
              <img src={microphone} height={20} alt="" className="mx-2" />
              <p className="m-0">
                <span className="fw-bold">Konsertstart</span>
              </p>
            </div>
            <div className="d-flex">
              <img src={location} height={20} alt="" className="mx-2" />
              <p className="m-0">
                <span className="fw-bold">Sted :</span>
                {event?.address}
              </p>
            </div>
            <br />
            <br />
            <div>
              <p style={{ whiteSpace: "pre-wrap" }}>{event?.description}</p>
            </div>
          </div>
          <div className="col-md-6 col-12">
            <div className="bg-secondary text-white p-5">
              <h3 className="fw-normal">Velg billetter (maks 30):</h3>
              <p className="fw-normal">
                Nedenfor finner du ledige billetter til dette arrangementet.
                Velg antall billetter du ønsker og klikk Kjøp nå. Du har 15
                minutter på deg til å fullføre bestillingen.
              </p>
              <ul>
                <li>
                  <div className="d-flex justify-content-between">
                    <div>
                      <span className="fs-4">Ordinær</span>
                      <br />
                      <span className="fs-6">({ordernum}) NOK</span>
                    </div>
                    <div>
                      <button
                        className="btn btn-outline-light"
                        disabled={ordinaryCount <= 0}
                        onClick={handleOrdinaryDecrease}
                      >
                        -
                      </button>
                      <span className="mx-3">{ordinaryCount}</span>
                      <button
                        className="btn btn-outline-light"
                        onClick={handleOrdinaryIncrease}
                        disabled={count === 30}
                      >
                        +
                      </button>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="d-flex justify-content-between">
                    <div>
                      <span className="fs-4">Student & U18</span>
                      <br />
                      <span className="fs-6">({studentnum}) NOK</span>
                    </div>
                    <div>
                      <button
                        className="btn btn-outline-light"
                        disabled={studentCount <= 0}
                        onClick={handleStudentDecrease}
                      >
                        -
                      </button>
                      <span className="mx-3">{studentCount}</span>
                      <button
                        className="btn btn-outline-light"
                        onClick={handleStudentIncrease}
                        disabled={count === 30}
                      >
                        +
                      </button>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="d-flex justify-content-between">
                    <div>
                      <span className="fs-4">Ledsager</span>
                      <br />
                      <span className="fs-6">({ledsagarnum}) NOK</span>
                    </div>
                    <div>
                      <button
                        className="btn btn-outline-light"
                        disabled={LedsagarCount <= 0}
                        onClick={handleLedsagarDecrease}
                      >
                        -
                      </button>
                      <span className="mx-3">{LedsagarCount}</span>
                      <button
                        className="btn btn-outline-light"
                        onClick={handleLedsagarIncrease}
                        disabled={count === 30}
                      >
                        +
                      </button>
                    </div>
                  </div>
                </li>
              </ul>
              <div className="mt-5">
                <h3>Delsum : {Delsum} kr</h3>
                <div className="input-group mb-3">
                  <input
                    type="text"
                    name="coupon"
                    className="form-control bg-secondary text-white"
                    placeholder="Coupon Code"
                    aria-label="Coupon Code"
                    aria-describedby="basic-addon2"
                    onChange={handleChange}
                    disabled={total <= 0}
                  />
                  <button
                    className="btn btn-light"
                    id="basic-addon2"
                    onClick={handleCouponApply}
                    disabled={coupon?.length <= 0}
                  >
                    Apply
                  </button>
                </div>
                <div className="text-end fst-italic fw-bold">
                  <p hidden={discountPrice === 0}>You Saved {discountPrice}</p>
                </div>

                <h3>Total : {total} kr</h3>
                <button
                  disabled={total <= 0}
                  className="btn btn-outline-light w-100"
                  onClick={handleCheckout}
                >
                  Kjøp nå
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Events;
