import { useCallback, useState, useEffect } from "react";
import { loadStripe } from "@stripe/stripe-js";
import {
  EmbeddedCheckoutProvider,
  EmbeddedCheckout,
} from "@stripe/react-stripe-js";
import { useLocation } from "react-router-dom";

const stripePromise = loadStripe(
  "pk_test_51LY5n9KJDWRkkrGG7jgeJzIjKNhzomeKAqgkt0bZCCfXnLkGHdqEHLRHeWOTPFd7piFwJItjhfm3DwkoQ0mfYHrJ001yj0OLsA"
);

const Checkout = () => {
  const [clientsecret, setClientSecret] = useState('');
  const location = useLocation();
  const data = location.state;

  console.log("data from checkout navigate state ",data)

  useEffect(() => {
    const fetchClientSecret = async () => {
      try {
        // Create a Checkout Session
        const res = await fetch(
          'https://api-test.mintix.no/create-checkout-session',
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
          }
        );

        const responseBody = await res.json();
        setClientSecret(responseBody.clientSecret);
      } catch (err) {
        console.log(err);
      }
    };

    fetchClientSecret();
  }, []); 

  return (
    <div>
      {clientsecret ? (
        <EmbeddedCheckoutProvider
          stripe={stripePromise} 
          options={{ clientSecret: clientsecret }}
        >
          <EmbeddedCheckout />
        </EmbeddedCheckoutProvider>
      ) : (
        <div>LOADING...</div>
      )}
    </div>
  );
};

export default Checkout;
