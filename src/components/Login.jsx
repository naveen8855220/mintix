import React, { useEffect, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import { auth } from "../services";
import { useNavigate } from "react-router-dom";
import { ValidateField, validateForm } from "../validations/Validations";
import { usemyStore } from "../store/store";

const Login = () => {
  const [user, setUser] = useState({});
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();
  const store = usemyStore((state) => state);
  const { isLogged, logged } = store;

  const handleSignIn = async (e) => {
    e.preventDefault();
    const { valid, errors: newErrors } = validateForm(user);
    setErrors(newErrors);

    if (errors?.username === "" && errors?.password === "") {
      const res = await auth?.signInUser(user);
      const result = res?.toJSON();
      console.log("result login details ", result);

      if (result?.username && result?.sessionToken) {
        console.log("user name ", result?.username);
        logged(true);
        localStorage.setItem("isLoggedIn", "true");
        navigate("/");
        // navigate(-1);
        // window.location.realod(true  )
      } else {
        console.log("Login Error ", result);
        logged(false);
        navigate("/login");
      }
    } else {
      console.log("validation error ", newErrors);
    }
  };

  const handleChange = (e) => {
    e.preventDefault();
    const { name, value } = e.target;
    setUser((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    //validate fields on input change
    const error = ValidateField(name, value);
    setErrors((prevState) => ({
      ...prevState,
      [name]: error,
    }));

    // console.log("error ",error)
    console.log("isLogged ", isLogged);
  };
  return (
    <div className="container py-5 my-5">
      <div className="row">
        <div className="col-md-4 offset-md-4">
          <h2 className="text-center">Welcome Back</h2>
          <input
            name="username"
            type="text"
            className="form-control py-3"
            placeholder="Username"
            onChange={handleChange}
          />
          {/* {errors?.username && <span>{errors?.username}</span>} */}
          <input
            name="password"
            type="password"
            className="form-control py-3"
            placeholder="Password"
            onChange={handleChange}
          />
          {/* {errors?.password && <span>{errors?.password}</span>} */}
          {errors?.username && errors?.password && (
            <span className="text-danger">
              Failed to log in.{errors?.username + " " + errors?.password}
            </span>
          )}
          {errors?.username && !errors?.password && (
            <span className="text-danger">
              Failed to log in.{errors?.username}
            </span>
          )}
          {errors?.password && !errors?.username && (
            <>
              <span className="text-danger">
                Failed to log in.{errors?.password}
              </span>
            </>
          )}
          <button
            onClick={handleSignIn}
            className="btn btn-outline-secondary py-3 w-100 fw-bold"
          >
            Login
          </button>
          <div className="py-3 text-center fs-5">
            <Link to={"/signup"} className="px-3 text-dark">
              Create an Account
            </Link>
            <Link to={"/reset-password"} className="px-3 text-dark">
              Forget Password
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
