import React, { useState } from "react";
import { auth } from "../services";
import { ValidateField, validateForm } from "../validations/Validations";

const Signup = () => {
  const [user, setUser] = useState({});
  const [errors, setErrors] = useState({});

  const handleSignUp = async (e) => {
    e.preventDefault();

    const { valid, errors: newErrors } = validateForm(user);
    setErrors(newErrors);

    if (
      errors?.username === "" &&
      errors?.email === "" &&
      errors?.password === ""
    ) {
      const res = await auth.signUpUser(user);
      console.log("signup response ", res);
    } else {
      console.log("signup dail");
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUser((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    //validate on input change
    const error = ValidateField(name, value);
    setErrors((prevState) => ({
      ...prevState,
      [name]: error,
    }));
  };
  console.log("errors from fignup ", errors);

  return (
    <div className="container py-5 my-5">
      <div className="row">
        <div className="col-md-4 offset-md-4">
          <h2 className="text-center">Create new Account</h2>
          <input
            type="text"
            name="username"
            className="form-control py-3"
            onChange={handleChange}
            placeholder="Username"
          />
          <input
            type="text"
            name="email"
            className="form-control py-3"
            onChange={handleChange}
            placeholder="Email"
          />
          <input
            type="password"
            name="password"
            className="form-control py-3"
            onChange={handleChange}
            placeholder="Password"
          />
          {errors?.username && errors?.email && errors?.password && (
            <span className="text-danger">
              Failed to log in.{errors?.username + " " + errors?.password}
            </span>
          )}
          {errors?.username && !errors?.password && (
            <span className="text-danger">
              Failed to log in.{errors?.username}
            </span>
          )}
          {errors?.email && !errors?.username && !errors?.password && (
            <>
              <span className="text-danger">
                Failed to log in.{errors?.email}
              </span>
            </>
          )}
          {errors?.password && !errors?.username && (
            <>
              <span className="text-danger">
                Failed to log in.{errors?.password}
              </span>
            </>
          )}

          <button
            onClick={handleSignUp}
            className="btn btn-outline-secondary py-3 w-100 fw-bold"
          >
            Sign Up
          </button>
        </div>
      </div>
    </div>
  );
};

export default Signup;
