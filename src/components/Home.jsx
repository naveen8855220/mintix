import React, { useEffect } from "react";
import Slider from "./Slider";
import Events from "./Events";
const Home = () => {

  useEffect(() => {
    const isLoggedIn = localStorage.getItem('isLoggedIn');
    if (isLoggedIn === 'true') {
      localStorage.removeItem('isLoggedIn'); 
      window.location.reload(); 
    }
  }, []);

  return (
    <>
      <Slider />
    </>
  );
};

export default Home;
