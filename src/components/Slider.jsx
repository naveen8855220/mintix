import React, { useEffect, useState } from "react";
import sliderImage from "../Images/slider.png";
import { api } from "../services";
import { useNavigate } from "react-router-dom";
import { usemyStore } from "../store/store";

const Slider = () => {
  const [event, setEvent] = useState([]);
  const navigate = useNavigate();
  const store = usemyStore((state) => state);
  const { isLogged, logged } = store;

  useEffect(() => {
    const fetchEvents = async () => {
      try {
        const res = await api.getEvents();
        setEvent(res);
      } catch (error) {
        console.log("fetchEvents Error ", error);
      }
    };

    fetchEvents();
  }, []);

  console.log("events ", event);

  const fetchDate = (item) => {
    const date = new Date(item);
    const dateString = date.toDateString();
    return dateString;
  };

  console.log("isLogged ", isLogged);

  return (
    <>
      <div className="container pb-5">
        <div
          id="carouselExample"
          className="carousel slide"
          data-bs-ride="carousel"
        >
          <div className="carousel-inner">
            {event?.map((item, i) => {
              return (
                <div
                  key={i}
                  className={`carousel-item ${i === 0 ? "active" : ""}`}
                >
                  <img
                    src={item?.cover_photo}
                    className="d-bock w-100"
                    alt={item?.title}
                  />
                </div>
              );
            })}
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExample"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExample"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>

      {/* Events */}

      <div className="container">
        <div className="row justify-content-center">
          {event?.map((item, index) => (
            <div
              key={item?.Id}
              onClick={() => {
                {
                  isLogged === true ? (
                    <>{navigate(`event/${item?.Id}`)}</>
                  ) : (
                    <>{navigate("/login")}</>
                  );
                }
              }}
              className="col-md-4 col-1"
            >
              <div className="cards shadow">
                <img
                  src={item?.cover_photo}
                  alt="Image"
                  className="card-img-top"
                />
                <div className="text-center fw-bold p-4">
                  <h4 className="fw-bold">{item?.title}</h4>
                  <p className="fw-normal">{fetchDate(item?.date)}</p>
                  <p className="fw-normal">{item?.address}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default Slider;
