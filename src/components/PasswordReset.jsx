import React, { useState } from "react";
import { ValidateField, validateForm } from "../validations/Validations";
import { passwordReset } from "../services/auth.service";
import { useNavigate } from "react-router-dom";

const PasswordReset = () => {
  const [email, setEmail] = useState("");
  const [errors, setErrors] = useState({});
  const [success, setSuccess] = useState(false);
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEmail(value);

    //validate fields on input change
    const error = ValidateField(name, value);
    setErrors((prevState) => ({
      ...prevState,
      [name]: error,
    }));
  };

  const handleReset = async (e) => {
    e.preventDefault();

    const { errors: newErrors } = validateForm(email);
    setErrors(newErrors);

    if (errors?.email === "") {
      console.log("reset email");
      const res = await passwordReset(email);
      setSuccess(true);
    } else {
      console.log("error ", errors?.email);
    }

    if (success) {
      navigate("/");
    }
  };

  return (
    <div className="container text-center">
      <div className="row">
        <div className="col-md-4 col-12 offset-md-4">
          <h2>Forget Password ?</h2>{" "}
          <input
            name="email"
            type="text"
            className="form-control py-3 my-3"
            placeholder="Username"
            onChange={handleChange}
          />
          {success && (
            <>
              <span>
                password change link sent succcessfully please check your
                mailbox
              </span>
            </>
          )}
          {errors?.email && (
            <p className="text-start text-danger p-0 m-0">{errors?.email}</p>
          )}
          <button
            onClick={handleReset}
            className="btn btn-outline-secondary py-3 w-100 fw-bold"
          >
            Reset Password
          </button>
        </div>
      </div>
    </div>
  );
};

export default PasswordReset;
